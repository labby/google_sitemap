<?php

/** ACKNOWLEDGMENTS ON BASED CODE AUTHOR AND MODS
 * Google Site Map
 * @author Karelkin Vladislav, Ruud, FreeSbee, erpe
 * @copyright 2007/2018 GPL
 * @version 2.0.2
 * @guid 103ab708-4fdb-46f9-917a-92ed6472aa59
 * @link https://support.google.com/webmasters/answer/183668
*/

// -------------------------------------------------------------------------
// CONFIGURATION
// -------------------------------------------------------------------------


// Set configuration values

// Debug information on / off
$debug               = false;

// Ban urls with unwanted words
$exclude             = array();		// Array of unwanted words in the url
									// eg. array("privat", "do-not-enter", "keep-away")


// Priorities in sitemap should be 0.5 if not set. Google is happy when different priorities are set.

// Normal Pages
$page_priority       = "0.5";		// Default priority for normal pages
$page_home_priority  = "1.0";  		// Homepage priority
$page_root_priority  = "0.6";  		// Toplevel menu pages priority
$page_frequency      = "weekly";	// Update frequency of your pages.
									// Allowed: always, hourly, daily, weekly, monthly, yearly, never

// News Module
$news_priority       = "0.7";		// News posts of the last 4 weeks
$news_old_priority   = "0.5";		// News posts older than 4 weeks
$news_frequency      = "weekly";  	// News posts update frequency

// Bakery Module
$bakery_priority     = "0.5";		//
$bakery_frequency    = "weekly";  	//

// Topics Module
$topics_mod_name     = "topics";	// Name of the module
$topics_priority     = "0.5";		//
$topics_frequency    = "weekly";  	//

// -------------------------------------------------------------------------
// END OF CONFIGURATION
// -------------------------------------------------------------------------


// Include config file
require_once(dirname(__FILE__)."/config/config.php");

// Check if the config file has been set-up
if(!defined("LEPTON_PATH")) {
	header("Location: install/index.php");
	exit(0);
}

// Create new frontend object
$oLEPTON = LEPTON_frontend::getInstance();
// Collect general website settings
$oLEPTON->get_website_settings();


// Vars
$counter    = 0;
$ts         = time();
$public     = array();
$modules    = array();
$debug_info = array();




// Functions
// *********

// Function check_link
function check_link($link, $exclude) {

	// Check for unwanted words in the url
	foreach ($exclude as $value) {
		if (strpos($link, $value)) {
			$unwanted = "&quot;".$link."&quot; contains &quot;".$value."&quot; and will not show up in the google sitemap\n";
			return $unwanted;
		}
	}

	// External links should not belong to the google sitemap
	if (strpos($link, '://')) {
		$unwanted = " ".$link." is an external link and will not show up in the google sitemap\n";
		return $unwanted;
	}
	return true;
}

// Function output_xml
function output_xml($link, $lastmod, $freq, $pri) {
echo '
  <url>
    <loc>'.$link.'</loc>
    <lastmod>'.$lastmod.'</lastmod>
	<changefreq>'.$freq.'</changefreq>
    <priority>'.$pri.'</priority>
  </url>';
}




// Start with xml header output
// ****************************

if ($debug) {
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8" />
	<style>
		url {
			 display: block;
		}
		loc {
			font-weight: bold;
			line-height: 25px;
		}
	</style>
</head>
<?php
} else {
	@header("Content-Type: application/xml");
	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo "\n";
	echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
}



// Get public pages
// *******************

// Get all pages from db except of the module menu_link
$result = array();
LEPTON_database::getInstance()->execute_query(
	"SELECT p.`link`, p.`modified_when`, p.`parent`, p.`position`, s.`section_id`, s.`module`
			FROM `".TABLE_PREFIX."pages` p
			JOIN `".TABLE_PREFIX."sections` s
			ON p.`page_id` = s.`page_id`
			WHERE (p.`visibility` = 'public')
				AND s.`module` != 'menu_link'
				AND (s.`publ_start` = '0' OR s.`publ_start` <= ".$ts.")
				AND (s.`publ_end` = '0' OR s.`publ_end` >= ".$ts.")
			ORDER BY p.`parent`, p.`position` ASC
			",
	true,
	$result,
	true
);

// Loop through the pages
if (count($result) > 0) {
	foreach ($result as $page) {
		$checked = check_link($page['link'], $exclude);

		if ($checked === true) {
			$link    = htmlspecialchars($oLEPTON->page_link($page['link']));
			$lastmod = gmdate("Y-m-d", $page['modified_when']);
			$freq    = $page_frequency;
			$pri     = $page_priority;
			if ($page['parent'] == 0) {
				if ($page['position'] == 1) {  
					$pri  = $page_home_priority;   // Should be the homepage
					$link = LEPTON_URL.'/';
				} else {
					$pri = $page_root_priority;    // Root level pages
				}
			}
			output_xml($link, $lastmod, $freq, $pri);
			$counter++;
			$public[]  = $page['section_id'];
			$modules[] = $page['module'];
		}
		else {
			$debug_info[] = $checked;
		}
	}
}

// All sections currently used modules
$modules = array_unique($modules);

// Count pages excluding module pages
$page_counter = $counter;




// Get module pages of previously set modules
// ******************************************

// News

if (in_array('news', $modules)) {
	$rs_news = array();
        LEPTON_database::getInstance()->execute_query(
            "SELECT `section_id`, `link`, `posted_when`, `published_when` FROM `". TABLE_PREFIX . "mod_news_posts` WHERE `active` = 1 
				AND (`published_when` = 0 OR `published_when` <= ".$ts.")
				AND (`published_until` = 0 OR `published_until` >= ".$ts.")
			",
            true,
            $rs_news,
            true
        );

	if (count($rs_news) > 0) {
		//echo (LEPTON_tools::display($rs_news,'pre','ui blue message'));
		foreach ($rs_news as $news) {
			if (!in_array($news['section_id'], $public)) continue;
			$checked = check_link($news['link'], $exclude);
			if ($checked === true) {
				$link     = htmlspecialchars($oLEPTON->page_link($news['link']));	
				$lastweek = time() - (4 * 7 * 24 * 60 * 60);
				if ($news['posted_when'] < $lastweek) { 
					$news_priority = $news_old_priority;
				}
				if ((version_compare(VERSION, '3.0.0') <= 0) && $news['published_when'] > 0){
					$lastmod = gmdate("Y-m-d", $news['published_when'] );
				} else {
					$lastmod = gmdate("Y-m-d", $news['posted_when'] );
				}
				output_xml($link, $lastmod, $news_frequency, $news_priority);
				$counter++;
			}
			else {
				$debug_info[] = $checked;
			}
		}
	}		
}


// Bakery
if (in_array('bakery', $modules)) {
	
	$rs_bakery = array();
        LEPTON_database::getInstance()->execute_query(
			"SELECT `section_id`, `link`, `modified_when`
						FROM `".TABLE_PREFIX."mod_bakery_items`
						WHERE `active` = '1'
			",
            true,
            $rs_news,
            true
        );	

	if (count($rs_bakery) > 0) {
		foreach ($rs_bakery as $bakery) {
			if (!in_array($bakery['section_id'], $public)) continue;
			$checked = check_link($bakery['link'], $exclude);
			if ($checked === true) {
				$link    = htmlspecialchars($oLEPTON->page_link($bakery['link']));
				$lastmod = gmdate("Y-m-d", $bakery['modified_when'] );
				output_xml($link, $lastmod, $bakery_frequency, $bakery_priority);
				$counter++;
			}
			else {
				$debug_info[] = $checked;
			}
		}
	}
}


// Topics
if (in_array($topics_mod_name, $modules)) {
	require(LEPTON_PATH.'/modules/'.$topics_mod_name.'/module_settings.php');
	$t = mktime ( (int) gmdate("H"), (int) gmdate("i"), (int) gmdate("s"), (int) gmdate("n"), (int) gmdate("j"), (int) gmdate("Y"));
	
	$rs_topics = array();
        LEPTON_database::getInstance()->execute_query(
			"SELECT `section_id`, `link`, `posted_modified`
				FROM `".TABLE_PREFIX."mod_".$topics_mod_name."`
				WHERE (`active` > '3' OR `active` = '1')
				AND (`published_when` = '0' OR `published_when` < ".$t.")
				AND (`published_until` = '0' OR `published_until` > ".$t.")
				ORDER BY `position` DESC
			",
            true,
            $rs_topics,
            true
        );		
	
	if(count($rs_topics) > 0) {
		foreach ($rs_topics as $topics) {
			if (!in_array($topics['section_id'], $public)) continue;
			$checked = check_link($topics['link'], $exclude);
			if ($checked === true) {
				$link    = htmlspecialchars(LEPTON_URL.$topics_directory.$topics['link'].PAGE_EXTENSION);
				$lastmod = gmdate("Y-m-d", $topics['posted_modified'] );
				output_xml($link, $lastmod, $topics_frequency, $topics_priority);
				$counter++;
			}
			else {
				$debug_info[] = $checked;
			}
		}
	}
}



// Add another module here...
// take any other module for example code



// Debug
if ($debug) {
	echo '<div style="display: block; white-space: pre; border: 2px solid #c77; padding: 0 1em 1em 1em; margin: 1em; line-height: 18px; background-color: #fdd; color: black">';
	echo '<h3>DEBUG</h3>';
	echo '<h3>Number of Pages</h3>';
	echo '<div style="font-family:monospace;font-size:12px">Number of Pages excluding module pages: '.$page_counter.'<br>';
	echo 'Number of all Pages including module pages: '.$counter.'</div>';
	if (count($debug_info) > 0) {
		echo '<h3>Banned Pages</h3><div style="font-family:monospace;font-size:12px">'.implode('', $debug_info).'</div>';
	}
	echo '</div>';
}
else {
	// End xml output
	echo "\n".'</urlset>';
}
